# frozen_string_literal: true

require 'semver_dialects/version'
require 'semver_dialects/semantic_version/version_translator'
require 'semver_dialects/semantic_version/version_parser'
require 'semver_dialects/semantic_version/version_range'

module SemverDialects
  # Captures all errors that could be possibly raised
  class Error < StandardError
    def initialize(msg)
      super(msg)
    end
  end

  # A utiltity module that helps with version matching
  module VersionChecker
    def self.version_translate(typ, version_string)
      case typ
      when 'maven'
        VersionTranslator.translate_maven(version_string)
      when 'npm'
        VersionTranslator.translate_npm(version_string)
      when 'conan'
        VersionTranslator.translate_conan(version_string)
      when 'nuget'
        VersionTranslator.translate_nuget(version_string)
      when 'go'
        VersionTranslator.translate_go(version_string)
      when 'gem'
        VersionTranslator.translate_gem(version_string)
      when 'pypi'
        VersionTranslator.translate_pypi(version_string)
      when 'packagist'
        VersionTranslator.translate_packagist(version_string)
      else
        raise SemverDialects::Error, 'unsupported package type'
      end
    end

    def self.version_sat?(typ, raw_ver, raw_constraint)
      version_constraint = version_translate(typ, raw_constraint)
      version = VersionParser.parse('=' + raw_ver)

      raise SemverDialects::Error, 'malformed constraint' if version_constraint.nil? || version_constraint.empty?
      raise SemverDialects::Error, 'malformed constraint' if version.nil? || version.empty?

      constraint = VersionRange.new
      version_constraint.each do |version_interval_str|
        constraint << VersionParser.parse(version_interval_str)
      end

      constraint.overlaps_with?(version)
    end
  end
end
