# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'semver_dialects/version'

Gem::Specification.new do |spec|
  spec.name          = 'semver_dialects'
  spec.license       = 'MIT'
  spec.version       = SemverDialects::VERSION
  spec.authors       = ['Julian Thome', 'Isaac Dawson', 'James Jonhson']
  spec.email         = ['jthome@gitlab.com', 'idawson@gitlab.com', 'jjohnson@gitlab.com']

  spec.summary       = 'This gem provides utility function to process semantic versions expressed in different dialects.'
  spec.description   = 'This gem helps to parse, process and compare semantic versions for Maven, NPM, PHP, RubyGems and python packages.'
  spec.homepage      = 'https://rubygems.org/gems/semver_dialects'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://rubygems.org"

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/semver_dialects'
    spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/semver_dialects/-/blob/master/CHANGELOG.md'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir["lib/**/*"]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'pastel', '~> 0.7.2'
  spec.add_dependency 'thor', '~> 0.20.0'

  spec.add_dependency 'tty-command', '~> 0.9.0'

  spec.add_development_dependency 'bundler', '~> 1.17'
  spec.add_development_dependency 'rake', '~> 12.3.3'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov', '~> 0.17.1'
end
