# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/semver_dialects/semantic_version/version_cut.rb'

RSpec.describe VersionTranslator do
  context 'Version Translation' do
    it 'for mvn should work correctly' do
      version_string = '(,3.2.9.RELEASE],[4.1-alpha0,4.1.3.RELEASE],[4.2-alpha0,4.2.0.RELEASE],(,6.6.6),[7.0.0,7.7.0)'
      version_array = VersionTranslator.translate_maven(version_string)
      expect do
        version_array == ['<=3.2.9.RELEASE',
                          '>=4.1-alpha0 <=4.1.3.RELEASE',
                          '>=4.2-alpha0 <=4.2.0.RELEASE',
                          '<6.6.6',
                          '>=7.0.0 <7.7.0']
      end
    end

    it 'for nuget should work correctly' do
      version_string = '(,3.2.9.RELEASE],[4.1-alpha0,4.1.3.RELEASE],[4.2-alpha0,4.2.0.RELEASE],(,6.6.6),[7.0.0,7.7.0)'
      version_array = VersionTranslator.translate_nuget(version_string)
      expect do
        version_array == ['<=3.2.9.RELEASE',
                          '>=4.1-alpha0 <=4.1.3.RELEASE',
                          '>=4.2-alpha0 <=4.2.0.RELEASE',
                          '<6.6.6',
                          '>=7.0.0 <7.7.0']
      end
    end

    it 'for npm should work correctly' do
      version_string = '<3.5.1 || >=4.0.0 && <4.1.3 || >=5.0.0 && <5.6.1 || >=6.0.0 && <6.1.2'
      version_array = VersionTranslator.translate_npm(version_string)
      expect do
        version_array == ['<3.5.1',
                          '>=4.0.0 <4.1.3',
                          '>=5.0.0 <5.6.1',
                          '>=6.0.0 <6.1.2']
      end
    end

    it 'for ruby should work correctly' do
      version_string = '>=2.3.0 <3.9.5 || >=3.10 <3.12 || >=4.0.0.preview2 <4.0.0.rc.2'
      version_array = VersionTranslator.translate_gem(version_string)
      expect do
        version_array == ['>=2.3.0 <3.9.5',
                          '>=3.10 <3.12',
                          '>=4.0.0.preview2 <4.0.0.rc.2']
      end
    end

    it 'for php should work correctly' do
      version_string = '<2.3.16||>=3.0.0-alpha,<3.0.10||>=3.1.0-alpha,<3.1.7||>=3.2.0-alpha,<3.2.7||>=3.3.0-alpha,<3.3.5'
      version_array = VersionTranslator.translate_packagist(version_string)
      expect do
        version_array == ['<2.3.16',
                          '>=3.0.0-alpha <3.0.10',
                          '>=3.1.0-alpha <3.1.7',
                          '>=3.2.0-alpha <3.2.7',
                          '>=3.3.0-alpha <3.3.5']
      end
    end

    it 'for python should work correctly' do
      version_string = '<1.11.1 || >=1.12.0,<1.12.2 || >=1.13.0,<1.13.2 || >=1.14.0,<1.14.2'
      version_array = VersionTranslator.translate_pypi(version_string)
      expect do
        version_array == ['<1.11.1',
                          '>=1.12.0 <1.12.2',
                          '>=1.13.0 <1.13.2',
                          '>=1.14.0 <1.14.2']
      end
    end
  end
end
