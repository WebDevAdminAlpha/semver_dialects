# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/semver_dialects/semantic_version/version_cut.rb'

RSpec.describe VersionCut do
  context 'Version comparison' do
    it 'should work for >' do
      expect(VersionCut.new('3') > VersionCut.new('2'))
    end

    it 'should work for <' do
      expect(VersionCut.new('2.3.1') < VersionCut.new('2.3.2'))
      expect(VersionCut.new('2.3.x') < VersionCut.new('2.3.7'))
      expect(VersionCut.new('2.4.0-alpha') < VersionCut.new('2.4.0-beta'))
      expect(VersionCut.new('3.0.0a1') < VersionCut.new('3.0.0RC4'))
      expect(VersionCut.new('1.3.0dev0') < VersionCut.new('1.3.0b3'))
      expect(VersionCut.new('1.2-alpha-6') < VersionCut.new('1.2-beta-2'))
      expect(VersionCut.new('2.1') < VersionCut.new('2.2'))
    end

    it 'should work for >=' do
      expect(VersionCut.new('2') >= VersionCut.new('2'))
      expect(VersionCut.new('2') >= VersionCut.new('1'))
    end

    it 'should work for <=' do
      expect(VersionCut.new('2') <= VersionCut.new('2'))
      expect(VersionCut.new('1') <= VersionCut.new('2'))
    end

    it 'should work for ==' do
      expect(VersionCut.new('4.2') == VersionCut.new('4.2.0.RELEASE'))
      expect(VersionCut.new('1') == VersionCut.new('1'))
    end
  end

  context 'version fiels' do
    it 'should be correctly separated' do
      vc0 = VersionCut.new('1.2.3')
      vc1 = VersionCut.new('1.2.0')
      expect(vc0.minor == '1')
      expect(vc0.major == '2')
      expect(vc0.patch == '3')
      expect(vc1.minor == '1')
      expect(vc1.major == '2')
      expect(vc1.patch == '0')
    end
  end

  context 'version diffing' do
    it 'should yield the correct differences' do
      vc0 = VersionCut.new('1.2.3')
      vc1 = VersionCut.new('1.2.0')
      vc3 = vc0.diff(vc1)
      expect(vc3.minor == '0')
      expect(vc3.major == '0')
      expect(vc3.patch == '3')
      expect(vc3.cross_total == 3)

      vc0 = VersionCut.new('1.2.3')
      vc1 = VersionCut.new('1.1.2')
      vc3 = vc0.diff(vc1)
      expect(vc3.minor == '0')
      expect(vc3.major == '1')
      expect(vc3.patch == '1')
      expect(vc3.cross_total == 2)
    end
  end

  context 'version diffing' do
    it 'should yield the correct differences with versions that have the same prefix' do
      vc0 = VersionCut.new('1.2')
      vc1 = VersionCut.new('1.2.0')
      vc3 = vc0.diff(vc1)
      expect(vc3.minor == '0')
      expect(vc3.major == '0')
      expect(vc3.patch == '0')
      expect vc3.cross_total.zero?
    end
  end

  context 'version diffing' do
    it 'should yield the correct differences with versions that are negative' do
      vc0 = VersionCut.new('1.1.2')
      vc1 = VersionCut.new('1.2.3')
      vc3 = vc0.diff(vc1, false)
      expect(vc3.minor == '0')
      expect(vc3.major == '-1')
      expect(vc3.patch == '-1')
      expect(vc3.cross_total == -2)
    end
  end

  context 'version cuts' do
    it 'should correctly identify initial versions' do
      vc0 = VersionCut.new('0.0.0')
      expect vc0.is_initial_version?
      vc1 = VersionCut.new('0')
      expect vc1.is_initial_version?
      vc2 = VersionCut.new('1.2.3')
      expect !vc2.is_initial_version?
      vc3 = VersionCut.new('0.rc1')
      expect vc3.is_initial_version?
    end
  end
end
