# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/semver_dialects/semantic_version/version_interval.rb'

RSpec.describe VersionInterval do
  context 'Version interval operations' do
    it 'diff should work correctly' do
      vi1 = VersionParser.parse('=3.2.9.RELEASE')
      vi2 = VersionParser.parse('=3.2.8.RELEASE')
      vi3 = VersionParser.parse('=*')
      expect  vi1.distinct?
      expect  vi2.distinct?
      expect  !vi1.universal?
      expect  !vi2.universal?
      expect  vi3.universal?
      expect  !vi3.distinct?
      vi4 = vi1.diff(vi2)
      vi5 = vi2.diff(vi1)
      expect(vi4 == vi5)
      expect(vi5.minor == '0')
      expect(vi5.major == '0')
      expect(vi5.patch == '1')
      expect(vi5.cross_total == 1)
      expect vi2.diff(vi3).empty?
      vi6 = VersionParser.parse('>0.0.0-alpha')
      expect vi6.universal?
    end

    it 'join should work' do
      vi1 = VersionParser.parse('=3.2.8.RELEASE')
      vi2 = VersionParser.parse('=3.2.9.RELEASE')
      expect  vi1.joinable?(vi2)
      expect  !vi2.joinable?(vi1)
      vi3 = vi1.join(vi2)
      expect(vi3.start_cut == vi1.start_cut)
      expect(vi3.end_cut == vi2.start_cut)
      expect(vi3.intersect(vi2) == vi2)
      expect(vi3.intersect(vi1) == vi1)
    end

    it 'intersect should work' do
      vi1 = VersionParser.parse('=3.2.8.RELEASE')
      vi2 = VersionParser.parse('=3.2.9.RELEASE')
      expect  vi1.joinable?(vi2)
      expect  !vi2.joinable?(vi1)
      vi3 = vi1.join(vi2)
      expect(vi3.start_cut == vi1.start_cut)
      expect(vi3.end_cut == vi2.start_cut)
      expect(vi3.intersect(vi2) == vi2)
      expect(vi3.intersect(vi1) == vi1)
    end

    it 'union should work' do
      vi1 = VersionParser.parse('>1.0.0 <4.0')
      vi2 = VersionParser.parse('<5.0.0')
      expect(vi1.union(vi2) == VersionParser.parse('<5.0.0'))
      vi3 = VersionParser.parse('>=1.0.0 <4.0')
      vi4 = VersionParser.parse('>1.0.0 <=4.0')
      expect(vi3.union(vi4) == VersionParser.parse('>=1.0.0 <=4.0'))
      expect(vi4.union(vi3) == VersionParser.parse('>=1.0.0 <=4.0'))
    end

    it 'collapse should work' do
      vi1 = VersionParser.parse('>=1.0.0 <4.0')
      vi2 = VersionParser.parse('<5.0.0')
      expect(vi1.collapse(vi2) == VersionParser.parse('>=1.0.0 <5.0.0'))
      vi3 = VersionParser.parse('>1.0.0 <=4.0')
      vi4 = VersionParser.parse('>=1.0.0 <4.0')
      expect(vi3.collapse(vi4) == VersionParser.parse('>=1.0.0 <=4.0'))
      vi4 = VersionParser.parse('>1.6')
      vi5 = VersionParser.parse('<2.0')
      expect(vi4.collapse(vi5) == VersionParser.parse('>1.6 <2.0'))
      vi6 = VersionParser.parse('>=1.6')
      vi7 = VersionParser.parse('=1.7')
      expect(vi6.collapse(vi7) == VersionParser.parse('>=1.6 <=1.7'))
      vi8 = VersionParser.parse('>=4.2.0')
      vi9 = VersionParser.parse('<=4.2.12')
      expect(vi8.collapse(vi9) == VersionParser.parse('>=4.2.0 <=4.2.12'))
    end

    it 'invert should work' do
      vi1 = VersionParser.parse('>=2 <10')
      vi1_inv = vi1.invert
      expect(vi1_inv.size == 2)
      expect(vi1_inv[0] == VersionParser.parse('<2'))
      expect(vi1_inv[1] == VersionParser.parse('>=10'))
      vi2 = VersionParser.parse('=*')
      vi2_inv = vi2.invert
      expect  vi2_inv.empty?
    end
  end
end
