# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/semver_dialects/semantic_version/semantic_version.rb'

RSpec.describe SemanticVersion do

  context 'Semnatic Version' do
    it 'should be classified correctly as pre-release' do
      expect(SemanticVersion.new('2.0.1.rc1').pre_release?).to be true
      expect(SemanticVersion.new('2.0.1.rc1').post_release?).to be false
      expect(SemanticVersion.new('2.3.1a').pre_release?).to be true
      expect(SemanticVersion.new('1.10.0alpha+test').pre_release?).to be true
      expect(SemanticVersion.new('1-pre').pre_release?).to be true
      expect(SemanticVersion.new('1.10-beta').pre_release?).to be true
      expect(SemanticVersion.new('1.10.M1').pre_release?).to be true
    end

    it 'should be classified correctly as post-release' do
      expect(SemanticVersion.new('2.0.1.SP1').post_release?).to be true
      expect(SemanticVersion.new('2.0.1.SP200').pre_release?).to be false
    end

    it 'should be classified correctly as normal release' do
      expect(SemanticVersion.new('2.0.1').post_release?).to be false
      expect(SemanticVersion.new('2.0.1').pre_release?).to be false
    end
  end

  context 'Semnatic Version comparison' do
    it 'should work correctly for different operators' do
      expect(SemanticVersion.new('2.3.1') < SemanticVersion.new('2.3.2'))
      expect(SemanticVersion.new('2.3.1') >= SemanticVersion.new('2.3'))
      expect(SemanticVersion.new('2.3.1') == SemanticVersion.new('2.3.1'))
      expect(SemanticVersion.new('2.3.1') != SemanticVersion.new('2.3.2'))
      expect(SemanticVersion.new('2') < SemanticVersion.new('3'))
      expect(SemanticVersion.new('3') > SemanticVersion.new('2'))
      expect(SemanticVersion.new('2.3.x') > SemanticVersion.new('2.3.1'))
      expect(SemanticVersion.new('2.3.1a') < SemanticVersion.new('2.3.1rc1'))
      expect(SemanticVersion.new('2.0.1.rc1') > SemanticVersion.new('2.0.x'))
      expect(SemanticVersion.new('1.10.0alpha') < SemanticVersion.new('1.10rc1'))
      expect(SemanticVersion.new('1.10.0alpha+test') == SemanticVersion.new('1.10.0alpha+test'))
    end

    it 'should normalize versions correctly' do
      expect(SemanticVersion.new('1.2-beta-2').to_normalized_s == '1:2:-12:2')
      expect(SemanticVersion.new('1.2rc0').to_normalized_s == '1:2:-11:0')
      expect(SemanticVersion.new('1.2-alpha-6').to_normalized_s == '1:2:-13:6')
    end
  end
end
